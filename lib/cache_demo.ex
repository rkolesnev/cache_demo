defmodule CacheDemo do
  use Plug.Router

  # plug Plug.Logger
  plug(:match)
  plug(:dispatch)

  get "/ping" do
    send_resp(conn, 200, "pong")
  end

  get "/no_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_data())
  end

  get "/local_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_cached(123))
  end

  get "/replicated_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_cached_replicated(123))
  end

  match _ do
    send_resp(conn, 404, "route not found")
  end
end
