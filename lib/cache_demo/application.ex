defmodule CacheDemo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    topologies = Application.get_env(:libcluster, :topologies, [])

    children = [
      {Cluster.Supervisor, [topologies, [name: CacheDemo.ClusterSupervisor]]},
      {CacheDemo.LocalCache, []},
      {CacheDemo.ReplicatedCache, []},
      {Plug.Cowboy,
       scheme: :http, plug: CacheDemo, options: Application.get_env(:cache_demo, CacheDemo)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CacheDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
