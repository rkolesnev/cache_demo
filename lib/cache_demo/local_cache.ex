defmodule CacheDemo.LocalCache do
  use Nebulex.Cache,
    otp_app: :cache_demo,
    adapter: Nebulex.Adapters.Local
end
