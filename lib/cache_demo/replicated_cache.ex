defmodule CacheDemo.ReplicatedCache do
  use Nebulex.Cache,
    otp_app: :cache_demo,
    adapter: Nebulex.Adapters.Replicated,
    primary_storage_adapter: Nebulex.Adapters.Local
end
