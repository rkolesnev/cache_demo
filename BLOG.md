# Distributed cache in Elixir

[Elixir](https://elixir-lang.org/) is a modern language with first-class support for concurrency and distributed applications. Things that can be challenging in other languages can be done in Elixir with minimal effort. One of such things is a distributed cache without any additional infrastructure. Such an approach can reduce your infrastructure costs, provide excellent performance, and work well with cloud autoscaling.

This article's primary goal is to show the effort you need to bring a distributed caching solution to the Elixir app, and to provide some helpful hints and insights we found on the path.

## Motivation

We have a service that holds configurations. Our multiservice platform can request multiple configurations during one external request, and configuration requests can involve multiple requests to underlying storages like DynamoDB and others. Configurations, however, do not change often, so they can be an ideal subject for in-memory caching optimization.

In our team, we have a tradition of an "Innovation Day" each second week when we can freely experiment with our services, propose any improvements, and clean up annoying parts of the code. We can do any technical stuff that can be useful without any bureaucracy stoppers. During one of such days, a week before Black Friday, we made a prototype of distributed caching. We expect a significant load spike during Black Friday and Cyber Monday as a retail platform, and we realized that such caching could be a remarkable improvement to our system before this period. So we spent one more day polishing it and deployed it to production. It worked without any issues! As a result, our service started to return sub-millisecond responses!

Fortunately, our solution is pretty simple, so let me show you how you can enable distributed cache in your own Elixir project. In practice, you probably already have an application to speed up, but let's set up a simple HTTP service in Elixir before we proceed. I'll also shortly highlight a few essential details about Elixir and its ecosystem if you're not familiar with this technology yet.

## Prerequisite: HTTP Service with no caching

Elixir is a VM-based language. It uses [Erlang](https://www.erlang.org/)'s VM called BEAM. An instance of the BEAM is called _node_. One node can efficiently utilize all your CPU cores, so you don't usually need more than one node on one physical machine. In most cases, when you run your Elixir app locally, you start one VM node.

Elixir has first-class support for parallelism and utilizes the [Actor Concurrency Model](https://en.wikipedia.org/wiki/Actor_model) (actors are called processes in Erlang terminology). On top of that, Elixir follows Erlang/OTP conventions, and each Elixir app is a [Supervisor Tree](https://www.erlang.org/doc/design_principles/des_princ.html). Oversimplifying: an app consists of independent processes that work like small services; and supervisors that ensure that all processes are running and restart if needed. Yes, Elixir/Erlang runtime reminds a set of microservices organized by Kubernetes. If you are interested in learning and experimenting with Elixir, consider visiting [Official Getting Started Guide](https://elixir-lang.org/getting-started/introduction.html) or [Elixir School](https://elixirschool.com/en) resources.

This diagram visualizes what our simple HTTP server will look like (with a focus on the supervisor tree):

```plantuml
@startuml
package "BEAM VM" as BEAM {
    package "cache_demo app" {
        component "CacheDemo.Application" as App
        component "Plug.Cowboy (CacheDemo)" as Plug
        component "Short-living Actor for request 1" as Req1
        component "Short-living Actor for request 2" as Req2
        component "..." as ReqRest
        
        interface "TCP PORT" as TCP
        
        App --> Plug : starts and observes
        TCP .. Plug
        Plug --> Req1
        Plug --> Req2
        Plug --> ReqRest
        
        note right of App
            The root supervisor.
            Any Elixir app has a root supervisor,
            it ensures that child processes are working properly
            and restarts them on failure.
        end note

        note right of Plug
            This actor listens to incoming HTTP requests
            and creates separate actors to process each of them.
            
            Plug.Cowboy is a module from the plug_cowboy library
            that implements low-level details.
        end note
        
        note bottom of Req1
            In Elixir/Erlang creating a new process is a cheap operation.
            So it's ok to spawn a short-living process for each request.
        end note
    }
}
@enduml
```

In Elixir, code is organized in modules. A module is a named set of functions. By following some rules, a particular module can implement some behavior ("supervisor behavior", for example). Roughly speaking, you may consider that there are two types of modules: just sets of functions and process behavior implementations. Usually, each node in the supervisor tree is implemented as a separate module. Also, you can provide initialization arguments to it. On the diagram, we refer to [Plug.Cowboy](https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html), but we provide our module with routing implementation (`CacheDemo`).

This repository contains the final form of an application we're discussing: https://gitlab.com/rkolesnev/cache_demo (except the Kubernetes setup part).

To simplify testing, let's assume that we have some external data storage, and it takes 20ms to get data from it. Let's implement it in a simple `CacheDemo.Repo` module:

```elixir
defmodule CacheDemo.Repo do
  def get_data do
    Process.sleep(20)

    "THIS IS DATA!"
  end
end
```

And let's expose it via HTTP endpoints by defining a router:

```elixir
defmodule CacheDemo do
  use Plug.Router

  # plug Plug.Logger
  plug :match
  plug :dispatch

  get "/ping" do
    send_resp(conn, 200, "pong")
  end

  get "/no_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_data())
  end

  match _ do
    send_resp(conn, 404, "route not found")
  end
end
```

And make it alive by attaching to the root supervisor:

```elixir
defmodule CacheDemo.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Plug.Cowboy, scheme: :http, plug: CacheDemo, port: 9000}
    ]

    opts = [strategy: :one_for_one, name: CacheDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
```

Now we can call our endpoints. Let's use [hey](https://github.com/rakyll/hey) for benchmarking. As a benchmark, let's send 5k requests with a concurrency of 50. (Concurrency will affect our result significantly, so it's important to stick to the same value). `hey` will provide a lot of data. For simplicity, we will focus on two values: average time and [99th percentile](https://thepercentagecalculator.net/percentile-meaning/what-does-the-99th-percentile-mean.html). All the benchmarks in this article are done on the same machine in similar conditions.

This is our "no operation" endpoint. We _cannot be faster_ than it:

```
hey -n 5000 -c 50 http://127.0.0.1:9000/ping

> Average:	0.0010 secs
> 99% in 0.0033 secs
```

And this is our "not cached" endpoint. We _should not be slower_ than it:

```
hey -n 5000 -c 50 http://127.0.0.1:9000/no_cache

> Average:	0.0214 secs
> 99% in 0.0240 secs
```

Previously I told you about sub-millisecond responses, and you may be curious why even a no-op endpoint provides a 1ms response. The answer is concurrency. In production, we rarely have too many parallel requests. So, if we change the concurrency of `hey` to a number less than the amount of available CPU cores the results will be much more impressive (I have a 6-Core Intel Core i7 CPU):

```
hey -n 5000 -c 4 http://127.0.0.1:9000/ping

> Average:	0.0002 secs
> 99% in 0.0004 secs
```

## Step 1: Node Local Cache

Let's introduce node-local cache. In some similar situations, it'd be enough just to use [ETS](https://www.erlang.org/doc/man/ets.html) to solve this, which is an in-memory storage that can be used in a "key-value" manner. Since it is part of the Erlang's standard library, it would be reasonable to consider it as a default solution. But we will not use it because of 2 reasons:

* by using ETS, we need to handle expiration logic manually
* it cannot be easily upgraded to be a distributed solution

So, let's use a library that will handle these problems for us (it's based on ETS under the hood): [Nebulex](https://hexdocs.pm/nebulex/Nebulex.html). It has many features and can be used for a variety of cases. Even multilevel caches are possible with it. That makes it a flexible solution, and we can use the same library for different cases.

Moreover, overhead in addition to ETS is almost zero; we will see this in the first caching benchmark.

Our goal is the following supervisor tree:

```plantuml
@startuml
package "BEAM VM" as BEAM {
    package "cache_demo app" {
        component "CacheDemo.Application" as App
        component "Plug.Cowboy (CacheDemo)" as Plug
        component "CacheDemo.LocalCache" as Local
        component "Short-living Actor for request 1" as Req1
        component "..." as ReqRest
        
        interface "TCP PORT" as TCP
        
        App --> Local
        App --> Plug
        TCP .. Plug
        Plug --> Req1
        Plug --> ReqRest
        
        Req1 ..> Local : uses
        
        note right of App
            LocalCache should be started before HTTP server
            and stopped after.
        end note
        
        note right of Local
            Stores cache state,
            implements cache API.
        end note
    }
}
@enduml
```

This article is not a Nebulex usage manual, so it will not highlight some details and possibilities. But the real goal of the article is to show the amount of coding effort needed to spin up a cache. It's time to add cache! In `mix.exs`:

```elixir
# ...
  defp deps do
    [
      # ...
      {:nebulex, "~> 2.3"},
      {:decorator, "~> 1.4"} # it's an optional dependency for nebulex; skip it if you don't use decorators
    ]
  end
# ...
```

Then we have to create a module that represents cache:

```elixir
defmodule CacheDemo.LocalCache do
  use Nebulex.Cache,
    otp_app: :cache_demo,
    adapter: Nebulex.Adapters.Local
end
```

The module implements GenServer behavior. To make cache work, we have to add it to the supervisor tree of our app:

```elixir
defmodule CacheDemo.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {CacheDemo.LocalCache, []}, # <---------- order matters. See Supervisor docs.
      {Plug.Cowboy, scheme: :http, plug: CacheDemo, port: 9000}
    ]

    opts = [strategy: :one_for_one, name: CacheDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
```

We can configure our cache behavior in `config/*.exs` files:

```elixir
config :cache_demo, CacheDemo.LocalCache,
  # we don't use it, but let's enable it because in a complex project, it can be useful
  stats: true,
  telemetry: true
```

A configuration like this allows us to have different cache behavior for development, test, and production purposes. For example, you can set global TTL for development to a low value.

And now we can use cache in our repo (we're using decorators, but it's possible to interact with cache directly):

```elixir
defmodule CacheDemo.Repo do
  use Nebulex.Caching

  @ttl :timer.hours(1)
  
  # ...

  @decorate cacheable(cache: CacheDemo.LocalCache, opts: [ttl: @ttl])
  def get_cached(_id) do
    Process.sleep(20)

    "THIS IS DATA!"
  end
end
```

And expose to HTTP:

```elixir
defmodule CacheDemo do
  # ...

  get "/local_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_cached(123))
  end
  
  # ...
end

```

And benchmark it:

```
hey -n 5000 -c 50 http://127.0.0.1:9000/local_cache

> Average:	0.0011 secs (/ping result was 0.0010)
> 99% in 0.0033 secs (/ping result was 0.0033)
```

As we see, local cache usage hardly differs from the no-op endpoint. That's an **incredible** result already! It means that in most cases you don't even need to think about direct ETS usage for optimization reasons - library overhead is minimal!

Results like this are not possible with Redis and Memcached due to network communication and parsing overheads. Moreover, data in ETS is stored in a raw format, with no (de)serialization involved. So, you don't need to worry about things like "can my data be serialized properly?" and "will I have the same data after deserialization?". In suma, it makes ETS-based cache usage more performant, reliable, and easy to use than 3rd-party solutions.

On the other hand, 3rd-party solutions can be more convenient if you need a persistent cache.

## Step 2: Distributed Cache (local setup)

So far, we didn't do anything really impressive. We can achieve similar results by using some in-memory cache libraries for any popular language. But in this way, we will have a disadvantage: manual invalidation behavior. Nowadays, we rarely have apps installed just on one machine. We dynamically adjust a number of the app instances behind the load balancer in most cases. When we manually invalidate some cache - we invalidate it only on one instance. If we had to update the value in the cache, one instance would update it both in its own cache and the underlying storage, but the rest of the instances would still return the outdated value. To solve this problem, we should invalidate the cache on all instances at once.

A pretty popular solution is using an external cache like Memcached or Redis, but it will make our performance worse, add complexity to infrastructure, and introduce a new failure point. And let's not forget about serialization/deserialization complexity. Fortunately, in [OTP](https://www.erlang.org/doc/design_principles/des_princ.html)-powered languages like Erlang and Elixir, we have one more option: make our app distributed. All our instances will form a cluster where instances can communicate with each another. And with Elixir, it's surprisingly easy: the most tricky parts are already handled by OTP and the [libcluster](https://hexdocs.pm/libcluster/readme.html) library! Regarding cache - Nebulex already supports [distributed way](https://hexdocs.pm/nebulex/Nebulex.Adapters.Replicated.html) of working.

```plantuml
@startuml
package "BEAM VM 1" as BEAM {
    package "cache_demo app" {
        component "CacheDemo.Application" as App
        component "Plug.Cowboy (CacheDemo)" as Plug
        component "CacheDemo.LocalCache" as Local
        component "Short-living Actor for request 1" as Req
        component "..." as ReqRest
        
        interface "TCP PORT" as TCP
        
        App --> Local
        App --> Plug
        TCP .. Plug
        Plug --> Req
        Plug --> ReqRest
        
        Req ..> Local : uses
    }
}

package "BEAM VM 2" as BEAM2 {
    package "cache_demo app" as app2 {
        component "CacheDemo.Application" as App2
        component "Plug.Cowboy (CacheDemo)" as Plug2
        component "CacheDemo.LocalCache" as Local2
        component "Short-living Actor for request 1" as Req2
        component "..." as ReqRest2
        
        interface "TCP PORT" as TCP2
        
        App2 --> Local2
        App2 --> Plug2
        TCP2 .. Plug2
        Plug2 --> Req2
        Plug2 --> ReqRest2
        
        Req2 ..> Local2 : uses
    }
}

Local .. Local2 : replication of data

BEAM .. BEAM2 : connected into distributed VM 
@enduml
```

As the first step, let's set up a local cluster by running two app instances locally. In `mix.exs` add the dependency:

```elixir
  # ...
  defp deps do
    [
      # ...
      {:libcluster, "~> 3.3"}
    ]
  end
  # ...
```

Then configure `libcluster` to use local strategy (most probably it will be `config/dev.exs` file in your project): 

```elixir
config :libcluster,
  topologies: [
    local: [
      strategy: Cluster.Strategy.LocalEpmd
    ]
  ]
```

Start `libcluster`'s worker as a part of our app (`application.ex`):

```elixir
  # ...
  def start(_type, _args) do
    topologies = Application.get_env(:libcluster, :topologies, [])

    children = [
      {Cluster.Supervisor, [topologies, [name: CacheDemo.ClusterSupervisor]]},
      # ...
    ]
    # ...
  end
  # ...
```

To avoid port conflicts do not forget to make the port configurable via env variable:

```elixir
config :cache_demo, CacheDemo,
  port: System.get_env("PORT", "9000") |> String.to_integer()
```

Now we can start two instances of our application with REPL attached:

```sh
PORT=9000 iex --sname node-A -S mix run
PORT=9001 iex --sname node-B -S mix run
```

Check that nodes can see each other using `Node.list/0`:

```elixir
# on node-A:
iex(node-A@yourhost)1> Node.list()
[:"node-B@yourhost"]

# on node-B
iex(node-B@yourhost)1> Node.list()
[:"node-A@yourhost"]
```

That is all that we need to set up distributed VM environment locally! Let's make replicated cache:

```elixir
defmodule CacheDemo.ReplicatedCache do
  use Nebulex.Cache,
    otp_app: :cache_demo,
    adapter: Nebulex.Adapters.Replicated, # Replicated adapter instead of Local
    primary_storage_adapter: Nebulex.Adapters.Local # And we are using Local adapter to store a cache replica
end

# in application.ex
    children = [
      {Cluster.Supervisor, [topologies, [name: CacheDemo.ClusterSupervisor]]},
      {CacheDemo.LocalCache, []},
      {CacheDemo.ReplicatedCache, []}, # <---
      {Plug.Cowboy,
       scheme: :http, plug: CacheDemo, options: Application.get_env(:cache_demo, CacheDemo)}
    ]
    
# in config.ex let's use a similar config:
config :cache_demo, CacheDemo.ReplicatedCache,
  # we don't use it, but let's enable it because in a complex project, it can be useful
  stats: true,
  telemetry: true
  
# and add HTTP endpoint:
  get "/replicated_cache" do
    send_resp(conn, 200, CacheDemo.Repo.get_cached_replicated(123))
  end
```

Benchmark shows the same results as for local cache (!!!):

```
hey -n 5000 -c 50 http://127.0.0.1:9000/replicated_cache

> Average:	0.0011 secs (/ping result was 0.0010)
> 99% in 0.0035 secs (/ping result was 0.0033)
```

You can experiment with cache API and ensure that when you invalidate cache entry on one node, it's invalidated on another.
You can also check the behavior when your delete and add nodes to the cluster:

```elixir
# on node B:
iex(node-B@yourhost)1> CacheDemo.ReplicatedCache.get("my_key")
nil

# on node A:
iex(node-A@yourhost)1> CacheDemo.ReplicatedCache.get("my_key")
nil
iex(node-A@yourhost)2> CacheDemo.ReplicatedCache.put("my_key", "DATA")
:ok
iex(node-A@yourhost)3> CacheDemo.ReplicatedCache.get("my_key")
"DATA"

# on node B:
iex(node-B@yourhost)2> CacheDemo.ReplicatedCache.get("my_key")
"DATA"
iex(node-B@yourhost)3> CacheDemo.ReplicatedCache.delete("my_key")
:ok
iex(node-B@yourhost)4> CacheDemo.ReplicatedCache.get("my_key")
nil

# on node A:
iex(node-A@yourhost)4> CacheDemo.ReplicatedCache.get("my_key")
nil
```

At that moment, we were impressed by the amount of effort needed to spin up a _distributed_ cache with Elixir.

## Step 3: Make it work with Kubernetes

"But it works locally" - is a pretty common joke in the IT community. So, to tell about success, we have to deploy it to production. All you need is to configure `libcluster` to work well with your production environment. The library has [several adapters](https://hexdocs.pm/libcluster/readme.html#clustering) for different situations and also some [3-rd party ones](https://hexdocs.pm/libcluster/readme.html#third-party-strategies). This article aims to provide an idea of how much effort is needed to spin up distributed cache in the BEAM environment. To achieve this, let me illustrate what we needed to do to bring it to work in production.

We're using the Kubernetes cluster, and our final approach looks like this:

```plantuml
@startuml
cloud "Kubernetes API" as k8sapi
node "Docker container on pod 1" as pod1 {
    package "BEAM VM" as BEAM1 {
        component "cache_demo app" as app1
    }
}

node "Docker container on pod 2" as pod2 {
    package "BEAM VM" as BEAM2 {
        component "cache_demo app" as app2
    }
}

app1 ..> k8sapi : gets list of pod private IPs for deployment
app2 ..> k8sapi : gets list of pod private IPs for deployment

BEAM1 .. BEAM2 : discover each other by private IP
@enduml
```

We decided to use [`Elixir.Cluster.Strategy.Kubernetes` adapter](https://hexdocs.pm/libcluster/Cluster.Strategy.Kubernetes.html).
So, we put our configuration in `config/prod.exs`:

```elixir
config :libcluster,
  topologies: [
    kubernetes: [
      strategy: Elixir.Cluster.Strategy.Kubernetes,
      config: [
        mode: :ip,                        # this 2 options mean that "pod list" API call will
        kubernetes_ip_lookup_mode: :pods, # be used to determine pods' IP aderesses
        kubernetes_node_basename: "node-name",
        kubernetes_selector: "app=app-name", # selector for pods
        kubernetes_namespace: "our-k8s-namespace",
        polling_interval: 10_000
      ]
    ]
  ]
```

Each Kubernetes pod has an access token to access Kubernetes API. The library knows where to get this token and how to make a call. But we need to specify details like shown above.

We need some additional work on the infrastructure side to make this configuration work. It's essential to have proper node naming to avoid name clashes. So we have to change some things in the release setup. We're using [Elixir Releases](https://hexdocs.pm/mix/Mix.Tasks.Release.html) wrapped into a Docker image. To configure node naming in the `rel\env.sh.eex` add the following (execute [`mix release.init`](https://hexdocs.pm/mix/Mix.Tasks.Release.Init.html) if you do not have this folder):

```sh
export RELEASE_DISTRIBUTION=name
export RELEASE_NODE="app-name@${POD_IP}"
```

So, our nodes will be named after their private IPs. We control deployments via [Terraform](https://www.terraform.io/). To make things work, our service's deployment should be configured like this:

```terraform
locals {
  namespace = "our-k8s-namespace"
}

resource "kubernetes_deployment" "app-name" {
  metadata {
    name      = "app-name"
    namespace = local.namespace
  }

  spec {
    # ...

    # this is a pod spec template
    template {
      metadata {
        labels = {
          app = "app-name" # <--- this is essential for discovery new nodes (see kubernetes_selector in libcluster config)
        }
      }
      spec {
        # we need this because we have provide access to k8s' API "pod list" action
        service_account_name = kubernetes_service_account.app-name.metadata[0].name

        # ...

        container {

          # ...

          env { # this block is essential for proper node naming
            name = "POD_IP"
            value_from {
              field_ref {
                field_path = "status.podIP"
              }
            }
          }
          
          # ...

        }
      }
    }
  }
}
```

Then we have to configure service account:

```terraform
resource "kubernetes_service_account" "app-name" {
  metadata {
    name      = "app-name"
    namespace = local.namespace
    annotations = {
      # we're using k8s in AWS; your case can be different
      # so let me skip AWS role details.
      "eks.amazonaws.com/role-arn" = aws_iam_role.app-name.arn
    }
  }
}

resource "kubernetes_role" "app-name" {
  metadata {
    name      = "app-name"
    namespace = local.namespace
  }

  rule {
    # this is essential block!!!
    # It allowes libcluster to discover other nodes' private IPs via k8s API call.
    api_groups = ["*"]
    resources  = ["pods"]
    verbs      = ["list", "get"]
  }
}

resource "kubernetes_role_binding" "app-name" {
  metadata {
    name      = "app-name"
    namespace = local.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.app-name.metadata.0.name
  }
  subject {
    namespace = local.namespace
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.app-name.metadata.0.name
  }
}
```

And that's enough to make it work even with autoscaling. Nodes will join and leave the cluster automatically. But there is one more implicit but essential thing here:

> During the connection setup, after node names have been exchanged, the magic cookies the nodes present to each other are compared. If they do not match, the connection is rejected. The cookies themselves are never transferred, instead they are compared using hashed challenges, although not in a cryptographically secure manner.
>
> From [Erlang docs](https://www.erlang.org/doc/reference_manual/distributed.html#security)

In the case of Elixir releases, that "magic cookie" is [randomly generated](https://hexdocs.pm/mix/Mix.Tasks.Release.html#module-options) when we call `mix release`:

> `:cookie` - a string representing the Erlang Distribution cookie. If this option is not set, a random cookie is written to the `releases/COOKIE` file when the first release is assembled. At runtime, we will first attempt to fetch the cookie from the `RELEASE_COOKIE` environment variable and then we'll read the `releases/COOKIE` file.

In our case, it means that any two different versions of a docker image will have different cookies. So, when you do a rolling update - the cache will not be transferred between old and new pods. And that's good: we can safely change cache structure without worrying about compatibility with older deployments.

Also, in addition to things already highlighted, we made cache controllable by an environment variable. To make it easy to disable in the case of any problems. But we hadn't any issues.

## Conclusion

It took one day to make it work locally. One more day to adjust infrastructure to make pods discoverable for each other. As a result, our average response time reduced from ~7ms to submillisecond responses. I agree that introducing AWS managed Redis or Memcached would take similar time and effort, but our way has some valuable advantages:

- simpler infrastructure - no additional resources, only adjustments to existing ones
- no additional points of failure - cache is merely a part of your application
- no (de)serialization problems
- no network overhead for reaching the cache
- cache will be clear for new deployment by default

In Elixir, you can start REPL _inside_ working VM. So we can inspect what's inside the in-memory cache on a particular pod without any problems and without learning new tools or query syntax. We do not sacrifice debugging convenience here; we even made it simpler!

Of course, if we want a persistent cache - the situation will be different: for example, in Kubernetes, our services should be stateless, or we have to deal with a much more complex setup. But the described solution is perfect for wrapping external data sources in LRU (least recently used) manner.

And last but not least: we deployed this solution several months ago, and we have had no single problem with it since then.
