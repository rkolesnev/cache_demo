import Config

config :cache_demo, CacheDemo.LocalCache,
  # we don't use it, but let's enable it cause in a complex project it can be useful
  stats: true,
  telemetry: true

config :cache_demo, CacheDemo.ReplicatedCache,
  # we don't use it, but let's enable it cause in a complex project it can be useful
  stats: true,
  telemetry: true

config :cache_demo, CacheDemo, port: 9000

import_config("#{config_env()}.exs")
